// Moduł phonecatApp
var phonecatControllers = angular.module('phonecatControllers', []);

// Kontroler PhoneListCtrl - odpowiada za przechowywanie danych dot. telefonów
/*phonecatControllers.controller('PhoneListCtrl', ['$scope', '$http', function($scope, $http) {	
	$http.get('phones/phones.json').success(function(data) {
		$scope.phones = data;
	});
	$scope.order = 'age';	// ustawia domyślny sposób sortowania wyświetlanych telefonów
}]);

// Kontroler PhoneDetailCtrl - odpowiada za przechowywanie danych szczegółowych dot. telefonów
phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
	$http.get('phones/' + $routeParams.phoneId + '.json').success(function(data) {
		$scope.phone = data;
		$scope.mainImageUrl = data.images[0];
	});

	$scope.setImage = function(imageUrl) {
		$scope.mainImageUrl = imageUrl;
	};
}]);
*/

// scope pozwala na działanie na obiektach/zmiennych/tablicach w obrębie danego kontrolera

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone', function($scope, Phone) {
	$scope.phones = Phone.query();
  	$scope.orderProp = 'age';
}]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone', '$location', function($scope, $routeParams, Phone, $location) {
  		$scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
    	$scope.mainImageUrl = phone.images[0];
	});

  $scope.setImage = function(imageUrl) {
    	$scope.mainImageUrl = imageUrl;
  };

  $scope.backToPhones = function() {
  		$location.path( "/phones" );
  };
}]);